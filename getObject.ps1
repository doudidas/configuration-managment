# Parameters
param(
    [string]$cmd,
    [string]$verbose
)

# Connect to the source vRA instance
Get-Content .cached_session.json | ConvertFrom-Json | Set-Variable vRAConnection

Write-Output $cmd

$elements = Invoke-Expression $cmd
if ($verbose -ne 'silent') {
    Write-Output $elements
}


$folderPath = "export/$cmd"
 
If (!(test-path $folderPath)) {
    New-Item -ItemType Directory -Force -Path $folderPath
} else {
    Remove-Item $folderPath/*.json
}


foreach ($element in $elements) {
    if (Get-Member -inputobject $element -name "Name" -Membertype Properties) {
        $pathToFile = $folderPath + "/" + $element.Name + ".json"
        ConvertTo-json -InputObject $element -Depth 50 | Out-File -FilePath $pathToFile
    }
    elseif (Get-Member -inputobject $element -name "ID" -Membertype Properties) {
        $pathToFile = $folderPath + "/" + $element.ID + ".json"
        ConvertTo-Json -InputObject $element -Depth 50 | Out-File -FilePath $pathToFile
    }
}
